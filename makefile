TEXC=pdflatex
TEXF= 
TARGET=SDP
RM=rm -rf
OUTEXT=pdf
DELEXT= aux log out synctex.gz* toc bbl blg
DEPFILES= img img/clouds.jpg img/dhashtab.png img/persynch.jpg img/sockloop.jpg img/mom.jpg img/rpcbind.jpg img/bully_abc.jpg img/bully_de.jpg

$(TARGET).pdf: $(TARGET).tex $(DEPFILES) 
	make build
	make clean

img:
	mkdir img
img/clouds.jpg: img
	wget http://www.alexhost.ru/wp-content/uploads/2014/03/clouddiensteniaaspaassaasmetvoorbeelden.jpg -O img/clouds.jpg
img/dhashtab.png: img
	wget https://www.cs.rit.edu/~ark/summer2015/251/module12/fig03.png -O img/dhashtab.png
img/persynch.jpg: img
	wget http://csis.pace.edu/~marchese/CS865/Lectures/Chap4/Chapter4a_files/image006.jpg -O img/persynch.jpg
img/sockloop.jpg: img
	wget http://csis.pace.edu/~marchese/CS865/Lectures/Chap4/Chapter4a_files/image038.jpg -O img/sockloop.jpg
img/mom.jpg: img
	wget http://csis.pace.edu/~marchese/CS865/Lectures/Chap4/Chapter4a_files/image042.jpg -O img/mom.jpg
img/rpcbind.jpg: img
	wget http://csis.pace.edu/~marchese/CS865/Lectures/Chap4/Chapter4a_files/image036.jpg -O img/rpcbind.jpg
img/bully_abc.jpg: img
	wget http://csis.pace.edu/~marchese/CS865/Lectures/Chap6/Chapter6a_files/image059.jpg -O img/bully_abc.jpg
img/bully_de.jpg: img
	wget http://csis.pace.edu/~marchese/CS865/Lectures/Chap6/Chapter6a_files/image061.jpg -O img/bully_de.jpg

.PHONY: compile build clean reset
compile:
	$(TEXC) $(TEXF) $(TARGET).tex
build:
	for i in 1 2 3 ; do \
		make compile ; \
	done
clean:
	for ext in $(DELEXT) ; do \
		$(RM) $(TARGET).$$ext ; \
	done
	rm -rf ./cod/*.log
reset:
	$(RM) $(TARGET).$(OUTEXT)
	for file in $(DEPFILES) ; do \
		$(RM) $$file ; \
	done
	make clean
