\chapter{Sincronizzazione}
\section{Sincronizzazione degli Orologi}
In alcuni casi, è necessario in un sistema distribuito sincronizzarsi, non rispetto a degli eventi, ma rispetto al tempo: un problema è che gli orologi dei vari nodi possono essere leggermente diversi, il che rende difficile sincronizzarsi in base al tempo assoluto.

\subsection{Coordinated Universal Time}
L'UTC (\textit{Coordinated Universal Time}) è basato sul tempo calcolato dagli orologi atomici, il TAI (\textit{Temps Atomique International}): questo orario si ottiene come media dei tempi calcolati dai circa 50 orologi atomici in tutto il mondo.

A causa del rallentamento della velocità di rotazione terrestre, i giorni si stanno lentamente allungando: per questo al TAI vengono aggiunti ogni tanto dei \textit{leap second} per tenere l'orologio sincronizzato con il tempo solare. Il giorno 31 dicembre 2016 è stato aggiunto il ventisettesimo leap second, per cui il TAI (a differenza iniziale di 10 secondi nel 1972) differisce dall'UTC di 37 secondi.
\subsubsection{GPS}
I satelliti del GPS (\textit{Global Positioning System}) hanno degli orologi atomici (fino a 4 ciascuno), periodicamente ricalibrati. Ogni satellite invia periodicamente pacchetti contenenti la propria posizione e il timestamp di invio del pacchetto.
Questo consente ai ricevitori di calcolare la propria posizione a partire dai dati provenienti da tre satelliti: con un quarto è possibile calcolare l'errore del proprio clock locale.

\subsection{Network Time Protocol}
NTP (\textit{Network Time Protocol}) è un protocollo progettato per la sincronizzazione degli orologi delle macchine in una rete. Ogni nodo interroga periodicamente un \textit{time server} per ottenere il tempo accurato: il problema di questo approccio è che è necessario stimare accuratamente la latenza ed il round-trip time.

\subsubsection{Algoritmo di Berkeley}
Il time server (più propriamente il \textit{time daemon}) chiede attivamente ai nodi della rete qual è il loro orario: tra questi calcola una media dei tempi e comunica ad ogni nodo se deve accelerare o rallentare il proprio clock.

\subsection{Clock Logici}
La sincronizzazione per clock logici consiste nell'ottenere un ordinamento fra gli eventi in rete senza sincronizzare i clock fisici, ma con dei timestamp logici.
L'ordinamento desiderato è un ordine parziale tale che $a \prec b$ se\begin{itemize}
	\item $a$ e $b$ sono eventi di uno stesso processo e $a$ accade prima di $b$
	\item $a$ è l'invio di un messaggio e $b$ è la ricezione di tale messaggio
\end{itemize}
Essendo un ordine parziale, vale la proprietà transitiva: $ a\prec b \land b\prec c \Leftarrow a\prec c $\,. Per ottenere ciò, assegniamo ad ogni evento $e$ un timestamp $C(e)$, tale che $ C(a) < C(b) \Leftrightarrow a \prec b $\,.

\subsubsection{Algoritmo di Lamport}
L'algoritmo di Lamport serve per mantenere un insieme di clock logici consistenti senza avere un clock globale. Ogni processo $P_i$ mantiene un contatore locale $C_i$, che viene aggiornato come segue:\begin{itemize}
	\item Tra due eventi successivi nel processo $P_i$\,, $C_i$ viene incrementato di 1
	\item Ogni volta che il processo $P_i$ invia un messaggio $m$, il timestamp di $m$ è $ts(m)=C_i$
	\item Ogni volta che il processo $P_i$ riceve un messaggio $m$ aggiorna $C_i=max\graffe{ C_i\,,\,ts(m) }+1$
\end{itemize}
In caso di parità, viene prima l'evento con id di processo minore: se si considera questo metodo di risoluzione delle parità, si ottiene un ordine totale.

\section{Mutua Esclusione}
Servono degli algoritmi per garantire l'accesso alle risorse su cui è necessaria la mutua esclusione.
L'approccio centralizzato ricalca ciò che avviene in un sistema monoprocessore. Esiste un processo che svolge il ruolo di coordinatore. Quando un processo vuole accedere ad una risorsa, manda una richiesta al coordinatore. Se nessun altro processo sta utilizzando la risorsa, il coordinatore invia una risposta concedendo il permesso di accesso, altrimenti mette la richiesta in coda.

Questo approccio ha il problema di \textit{single-point-of-failure}: inoltre, per sistemi molto grandi, può rappresentare un collo di bottiglia. Un altro problema è che è difficile distinguere tra la non disponibilità della risorsa dalla non disponibilità del coordinatore.

\subsection{Algoritmo di Ricart-Agrawala}
L'algoritmo di Ricart-Agrawala è un algoritmo distribuito per la mutua esclusione: esso estende l'approccio presentato da Lamport ed è basato sull'esistenza di un ordinamento totale degli eventi.

Quando un processo vuole accedere a una risorsa, manda in broadcast un messaggio in cui comunica: l'id della risorsa $R$, il proprio pid $P_i$ ed il proprio valore di clock logico $C_i$.

Quando un processo $P_j$ riceve un messaggio\begin{itemize}
	\item Se non sta usando e non ha richiesto la risorsa $R$, risponde con un \textit{`OK'}
	\item Se sta usando la risorsa $R$ non risponde, ma inserisce la richiesta in coda
	\item Se non sta usando la risorsa $R$, ma la ha richiesta, vince chi l'ha chiesta prima. Se il clock della propria richiesta è $C_j$ \begin{itemize}
		\item Se $ (C_i\,,P_i) \prec (C_j\,,P_j) $, risponde con un \textit{`OK'}
		\item Se $ (C_i\,,P_i) \succ (C_j\,,P_j) $, non risponde, ma inserisce la richiesta in coda
	\end{itemize}
\end{itemize}

Il processo $P_i$ attende che ogni processo gli risponda, dopodiché procede con l'esecuzione. Quando rilascia la risorsa, manda un messaggio di \textit{`OK'} a tutti i processi nella propria coda, rimuovendoli dalla coda.

Questo approccio amplifica i problemi dell'algoritmo centralizzato: se prima avevamo un \textit{single-point-of-failure} nel coordinatore, qui ogni nodo è un \textit{single-point-of-failure}, in quanto le mancate risposte da parte di un nodo bloccano tutte le risorse. Questo si può risolvere introducendo degli acknowledgement negativi. Allo stesso modo, ogni nodo deve gestire il flusso di messaggi, il che può costituire un collo di bottiglia; inoltre, i messaggi sono molti di più e potrebbero contribuire a congestionare la rete.

Un problema aggiuntivo è il mantenimento della lista dei nodi nella rete per la comunicazione in broadcast. Tale comunicazione funziona in reti piccole o poco dinamiche, oppure se esistono delle primitive di rete per il multicast.

\subsection{Algoritmo Token Ring}
L'algoritmo \textit{token ring} funziona su un sistema con overlay ad anello: se i nodi sono $n$, il nodo $i$ comunica con il nodo $[i+1]_n$\,. Si fa girare un messaggio \textit{token} per ogni risorsa, chi riceve il token\begin{itemize}
	\item se non desidera usare la risorsa, lo passa al nodo successivo
	\item se desidera usare la risorsa può accedervi e trattenere il token, dopodiché rilascia la risorsa e passa il token
\end{itemize}
I problemi di questo approccio sono legati alla possibilità di perdere il token o di avere token duplicati.
I costi per gli algoritmi descritti sono\\\ \\\noindent\begin{tabular}{l|cc|cc}
	Algoritmo & \multicolumn{2}{c|}{Messaggi per acquisizione e rilascio} & \multicolumn{2}{c}{Ritardo nell'acquisizione (messaggi)} \\\hline&&&&\\
	&& Richiesta (1) && Richiesta (1) \\
	Centralizzato & 3 & Conferma (1) & 2 \\
	&& Rilascio (1) && Conferma (1) \\&&&&\\
	&& Richiesta $(n-1)$&& Richiesta $(n-1)$ \\
	Ricart-Agrawala & $3\,(n-1)$ & Conferma $(n-1)$ & $2\,(n-1)$ \\
	&& Rilascio $(n-1)$&& Rilascio $(n-1)$ \\&&&&\\
	& \multicolumn{2}{c|}{(Ho il token $\rightarrow$ 1)} & \multicolumn{2}{c}{(Ho il token $\rightarrow$ 0)} \\
	Token Ring & \multicolumn{2}{c|}{$1\sim +\infty$} &  \multicolumn{2}{c}{$0\sim n-1$} \\
	& \multicolumn{2}{c|}{(Nessuno vuole il token $ \rightarrow +\infty$)} & \multicolumn{2}{c}{(Il successivo ha il token $\rightarrow n-1$)}
\end{tabular}

\section{Elezione}
Alcuni algoritmi necessitano di un coordinatore: in un sistema decentralizzato si può stabilire dinamicamente quale processo è il coordinatore tramite elezione.

\subsection{Algoritmo del Bullo}
\begin{figure} \centering
	\caption{Elezione tramite bullismo: il nodo 4 avvia l'elezione mandando il messaggio a 5, 6 e 7, che è irraggiungibile. I nodi 5 e 6 mandano un \textit{`OK'} a 4 e un messaggio di elezione 7: il nodo 5 manda anche un messaggio di elezione a 6. Il nodo 6 manda un \textit{`OK'} a 5 e non riceve messaggi di elezione: diventa così il coordinatore e lo comunica a tutti}
	\label{fig:bully}
	\ \newline
	\includegraphics[height=0.16\textheight]{img/bully_abc.jpg}
	\includegraphics[height=0.15\textheight,trim={0 -0.13in 0 0.08in},clip]{img/bully_de.jpg}
\end{figure}
L'algoritmo del bullo (o \textit{Election by Bullying}) prevede che ogni processo abbia un peso associato: il processo con peso massimo viene eletto come coordinatoe. Un esempio di peso univoco e adatto allo scopo si può ottenere tramite concatenazione tra il reciproco del carico del processo (il processo più scarico viene eletto), il suo indirizzo IP e il suo PID.
%$$ w(P_i) = \left\langle \frac{1}{load(P_i)}\,,\, \text{IP}(P_i)\,,\,\text{PID}(P_i) \right\rangle  $$
Qualsiasi processo può avviare un'elezione inviando un messaggio di elezione a tutti gli altri processi: se un processo riceve un messaggio di elezione, manda un messaggio di \textit{`OK'} ai processi più leggeri e un messaggio di elezione ai processi più pesanti.
Quando un processo non riceve messaggi di \textit{`OK'} vince le elezioni e lo comunica a tutti.

\subsection{Algoritmo di Chang-Roberts}
L'algoritmo di Chang-Roberts per l'elezione ad anello funziona sfruttando un array ad anello in cui ogni nodo conosce il proprio successore ed il successore del proprio successore: il processo con peso massimo deve essere eletto come coordinatore.
\lstinputlisting{./cod/changroberts.code}
Questo algoritmo consente di limitare il numero di messaggi circolanti e di evitare conflitti in caso di più elezioni iniziate contemporaneamente. Inoltre, fornisce un meccanismo di robustezza ai guasti facilmente estendibile aumentando il numero di successori memorizzati da ogni nodo.
