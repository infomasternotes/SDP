\chapter{Sistemi Pervasivi}
Un sistema pervasivo è un sistema distribuito con nodi non convenzionali (quali possono essere sensori o \textit{smart object}), adattivo e ad alta volatilità (i nodi possono essere poco robusti, così come le reti di comunicazione, che sono anche molto dinamiche).

Gli smart objects sono dispositivi non finalizzati al calcolo (al contrario di un computer), ma che dispongono comunque di capacità di elaborazione, oltre ad una connessione ad Internet, con o senza un indirizzo IP.

Una problematica tipica di un sistema pervasivo è l'ottimizzazione energetica: infatti, molti di questi sistemi fanno affidamento su batterie, per cui monitorare e minimizzare il consumo energetico può risultare vitale per il funzionamento.

Un sistema pervasivo deve essere il più trasparente possibile: ciò vuol dire, tipicamente, minimizzare l'interazione esplicita con l'utente umano, tramite servizi \textit{context-aware}.

\section{Sistemi a Sensore}
Molti sistemi pervasivi integrano nodi dotati di sensori o attuatori per acquisire dati e interagire con l'ambiente.

\subsection{Reti di Sensori}
Le reti utilizzate nei sistemi pervasivi di sensori devono essere progettate in modo tale che si possa determinare in modo dinamico l'architettura di rete in modo che sia il più efficiente possibile e che risponda alle variazioni nei nodi connessi: deve poter essere molto semplice entrare nella rete per un nuovo nodo, così come uscire, inoltre la rete deve saper rispondere efficacemente al fallimento di un nodo.

Per robustezza ed efficienza, le reti utilizzate sono reti \textit{mesh}: in questi tipi di reti i sensori non comunicano solo con dei gateway, ma anche con altri sensori.

Le reti possono implementare degli algoritmi per l'integrazione dei dati provenienti dai sensori, ad esempio, con topologie di rete ad albero.

Gli standard per le reti di sensori sono Zigbee e Z-Wave, oltre al BLE (\textit{Bluetooth Low Energy}, detto anche \textit{Bluetooth Smart}) o al WiFi e, in futuro, a reti basate su 5G.

Altri problemi delle reti di sensori sono il \textit{discovery} ed il \textit{pairing}: servono dei meccanismi di \textit{network bootstrapping} per permettere ai dispositivi di registrarsi in una rete e acquisire un indirizzo univoco. Inoltre, è necessario specificare una modalità di associazione del dispositivo all'applicazione corretta.

\subsection{Dispositivi a Sensore}
I sensori utilizzati nei sistemi pervasivi possono essere fisici o virtuali: i sensori fisici sono dispositivi in grado di misurare grandezze fisiche e trasformarle in segnali, tipicamente digitali, i sensori virtuali, invece, sono servizi e app che forniscono informazioni riguardo ad un contesto, ad esempio, integrando dati da altri sensori.

I sensori più utilizzati al giorno d'oggi sono MEMS (\textit{MicroElectroMechanical Systems}): essi sono sistemi elettromeccanici miniaturizzati, che sono facilmente inseribili in un sistema embedded.

I sistemi pervasivi possono contenere dispositivi dotati di attuatori: al contrario dei sensori, gli attuatori sono trasduttori che permettono di trasformare un segnale elettrico in un'azione. Gli attuatori sono quasi esclusivamente attivi e necessitano di alimentazione.

I componenti principali di un \textit{sensor device} sono:\begin{itemize}
	\item Sottosistema di \textit{sensing}: dotato di un sensore per acquisire dati dall'ambiente
	\item Sottosistema di processing: dotato di un microprocessore per elaborare dati in locale
	\item Sottosistema di comunicazione: dotato di interfacce di comunicazione wireless o wired
	\item Sottosistema di storage: dotato di memorie di massa
	\item Sottosistema di alimentazione: batteria, pannelli solari, \dots 
\end{itemize}
I processori utilizzati nei dispositivi a sensore sono solitamente a basso consumo energetico, come i processori della famiglia ARM Cortex-M.

In un sistema distribuito di sensori sono necessarie delle \textit{base station} o degli \textit{hub} che hanno il compito di raccogliere dati dai sensori per poi trasmetterli ai nodi di calcolo (locali, oppure server remoti o cloud). Essi possono fungere anche da gateway verso altre reti, ad esempio, permettendo l'accesso in Internet a dispositivi sprovvisti di indirizzo IP. Inoltre, possono anche implementare meccanismi di cache, diventando un proxy per i sensori, ad esempio, memorizzando informazioni utili in un database embedded.

\subsection{Gestione dei Dati da Sensori}
I dati dai sensori sono un flusso più o meno continuo di informazione, per cui, la gestione di tale informazione è di natura diversa rispetto ai database tradizionali. Le interrogazioni sono solitamente delle \textit{continuous query}: l'informazione richiesta è in costante mutamento, per cui, ogni volta che il dato cambia viene trasmesso all'utente. Inoltre, i dati da sensori hanno una caratterizzazione spazio-temporale molto forte, di cui si dovrà tenere conto per la gestione delle interrogazioni.

\subsubsection{Metodi Base}
\paragraph{Batch Processing}
Le tecniche di elaborazione batch prevedono che i sensori non ricoprano alcun ruolo computazionale: essi inseriscono in dei buffer i dati che producono e questi vengono inviati alla base station. La base station (o il server), quindi, riceve tutti i dati e li elabora. Queste tecniche danno informazioni precise, ma hanno elevati costi di comunicazione.

\paragraph{Sampling}
I dati ottenuti da sensori sono potenzialmente continui: non è necessario, però, leggerli ed inviarli tutti. Le tecniche di campionamento prevedono che il sensore legga il segnale ad intervalli di tempo e invii solo quei campioni alla base station. Queste tecniche possono introdurre degli errori, ma si può dare una maggiorazione abbastanza accurata sull'errore introdotto.

\paragraph{Sliding Window}
Il dispositivo calcola delle statistiche sui dati basandosi su una finestra temporale di letture consecutive: queste finestre possono variare per lunghezza (più lunghe sono più calcoli deve compiere il dispositivo, ma meno messaggi deve inviare) e per overlap (la sovrapposizione tra due finestre consecutive). La base station riceve, così, meno dati e già in parte elaborati.
Esempi di calcoli da effettuare su finestre sono: media, varianza, minimo, massimo, fft, \dots 

\paragraph{Histogram}
Ciò che viene calcolato è l'istogramma delle letture: i bin sono decisi dalla base station, possibilmente dinamicamente per rispondere alle variazioni del fenomeno misurato.

\subsubsection{Metodi Avanzati}
Esistono dei metodi più avanzati, orientati al risparmio energetico e alla riduzione degli errori. Si tenga presente che la trasmissione è molto più costosa dell'elaborazione: trasmettere 1 bit consuma la stessa energia di 1000 operazioni su CPU.
\paragraph{In-Network Query Processing}
Le tecniche di \textit{in-network query processing} si basano sulla strutturazione della rete di sensori in un overlay che permetta l'aggregazione dei dati nei nodi intermedi, per ridurre i costi di trasmissione: ad esempio, in un overlay ad albero ogni parent può aggregare le informazioni dei propri figli.

\paragraph{Duty Cycling}
Dato che tenere accesi gli apparati di comunicazione è costoso, è possibile un imporre ai nodi un ciclo \textit{sleep/wake-up} coordinato: sarà necessario un algoritmo di scheduling e di sincronizzazione.

\paragraph{Mobility-Based}
La base station può essere un apparato mobile, così come i nodi, che possono essere dispositivi wearable: sfruttare la mobilità per redistribuire la trasmissione tra i nodi può portare molti vantaggi in termini di risparmio energetico.

\paragraph{Model-Based}
Alcune tecniche possono fare uso di modelli del fenomeno misurato per ridurre l'informazione da trasmettere: ad esempio, se il valore misurato corrisponde al valore atteso del modello (entro un margine di tolleranza), non deve essere trasmesso.
Tecniche basate su modelli possono essere usate anche per la riduzione del rumore e l'eliminazione degli outlier (\textit{data cleaning}).

Un esempio di compressione model-based è la compressione \textit{mid-range}: le letture di un intervallo temporale sono approssimate con il loro valore mid-range (media tra massimo e minimo). Per ogni campione si valuta se l'inserimento nell'intervallo corrente provoca un errore di predizione maggiore o minore di $\epsilon$: se l'errore massimo è minore di $\epsilon$ si può accodare all'intervallo corrente, altrimenti il campione è il primo elemento di un nuovo intervallo. In questo modo si dà la garanzia di un errore sempre minore di $\epsilon$.
Altri modelli di compressione sono i modelli di regressione come i modelli di regressione polinomiale.

La comunicazione dei dati da sensori può essere \textit{push} o \textit{pull}. Nell'acquisizione dati push, la base station e i sensori hanno un modello condiviso del comportamento atteso del fenomeno: i sensori dovranno comunicare solo i valori che deviano dal modello (per ridurre l'informazione trasmessa, possono comunicare anche solo la differenza dal valore atteso). La comunicazione pull, invece, prevede che sia l'utente a definire la frequenza di campionamento.

\section{Context Awareness}
Le applicazioni tradizionali non sono in grado di comprendere il contesto di una richiesta, per cui l'utente deve specificare tutti i parametri necessari per un'interrogazione: questo va a scapito della trasparenza del sistema, perché obbliga ad avere una interazione complessa con l'utente. L'adattività diventa, quindi, una proprietà fondamentale di un sistema pervasivo.

Ad esempio, in un software tradizionale, se l'utente vuole ottenere l'itinerario migliore per andare in un luogo, dovrà chiedere\begin{center}
	``Qual è la strada migliore per andare da \textit{X} a \textit{Y}, in macchina,\\considerando che è l'ora di punta e non voglio andare in autostrada?''
\end{center}
Un sistema adattivo potrebbe inferire\begin{itemize}
	\item \textit{X} dalla posizione attuale del dispositivo mobile
	\item \textit{Y} dal luogo del prossimo impegno nel calendario
	\item Le condizioni del traffico da Google Maps
	\item Il mezzo di trasporto dal fatto che lo smartphone è collegato in Bluetooth ad un auto
\end{itemize}
In questo modo l'utente può semplicemente chiedere\begin{center}
	``Qual è la strada migliore, senza andare in autostrada?''
\end{center}

\subsection{Contesto}
I dati di contesto si possono dividere in base all'entità che riguardano \begin{itemize}
	\item Utente: identità, dati fisiologici, emotivi, sociali, interessi preferenze, attività, \dots 
	\item Ambiente: condizione atmosferica, temperatura, umidità, luce, rumore, qualità dell'aria, persone, dispositivi, \dots 
	\item Località: fisica o simbolica
	\item Tempo
	\item Dispositivo: funzionalità, stato (batteria, \dots )
	\item Connettività
\end{itemize}
Inoltre, è molto importante il contesto temporale: tenere traccia della \textit{context history} permette di derivare nuovi dati di contesto e di predire il contesto futuro.

\subsection{Adattività}
L'adattività è la capacità di comprendere e adattarsi al contesto per fornire un miglior servizio: un sistema context-aware acquisisce dati di contesto continuamente per adattarsi. È una proprietà fondamentale per i sistemi mobili e pervasivi, che devono saper rispondere a variazioni nella connessione di rete, nella disponibilità di energia, nella situazione e nell'ambiente di utilizzo, oltre che limitare le interfacce e il numero di parametri da specificare per le richieste.

Esistono due principali tipi di adattività: adattività delle funzionalità e adattività dei dati. L'adattività delle funzionalità consiste nella regolazione adattiva del flusso di esecuzione, della precisione dei calcoli, della distribuzione del carico di lavoro e del caching, oltre che nella possibilità di nascondere o esporre funzionalità all'utente. L'adattività dei dati, invece, consiste nella regolazione della qualità, della precisione e della frequenza del flusso dati.

\subsection{Livello di Contesto}
I dati di contesto possono essere di basso o di alto livello. Il contesto di basso livello è ottenuto tramite acquisizioni dirette (dati raw da sensori, preferenze indicate dall'utente, specifiche comunicate da un dispositivo, \dots ) o da semplici elaborazioni di queste (misure indirette, statistiche, categorizzazioni semplici, \dots ).

Il contesto di alto livello, invece, è ottenuto applicando inferenze arbitrariamente complesse ai dati di basso livello: essi possono essere l'attività o la situazione emotiva di un utente, riconosciuta integrando dati da diversi sensori. I metodi di inferenza applicabili per derivare il contesto di alto livello si distinguono in metodi simbolici (modelli logici, \dots ) e metodi statistici (machine learning, \dots): è possibile adottare approcci ibridi, come modelli logici probabilistici o metodi statistici con conoscenza aprioristica codificata in forma logica.

\subsection{Rappresentazione del Contesto}
I dati di contesto sono ottenuti da sorgenti eterogenee e utilizzati da applicazioni diverse: si rende necessaria la definizione di un modello di rappresentazione del contesto per l'elaborazione automatica dei dati.
Un modello di rappresentazione del contesto dovrebbe supportare: tipi di contesto diversi, relazioni tra dati di contesto, context history, incertezza del contesto, ragionamento. Inoltre dovrebbe essere facilmente usabile ed efficiente.

\paragraph{Flat Models} I modelli \textit{flat} sono sviluppati ad hoc per un'applicazione e sono basati su coppie chiave/valore o su linguaggi di markup come XML o JSON. Essi danno qualche supporto per l'eterogeneità e l'estensibilità, ma non per caratteristiche più avanzate

\paragraph{DB Models} I modelli per la specifica di database (concettuali, logici, \dots) possono essere utilizzati per definire vari tipi di dati di contesto e le relazioni tra di essi: la modellazione è resa più semplice grazie a strumenti grafici e l'interrogazione e il ragionamento sono facilitati dall'applicazione di metodi formali. Un esempio è CML (\textit{Context Modelling Language}): una notazione grafica per modellare classi e sorgenti di dati di contesto e le loro dipendenze reciproche; supporta anche la cronologia e l'incertezza. I modelli CML possono essere implementati in un database relazionale.

\paragraph{Modelli Ontologici} Per ontologia si intende una rappresentazione formale e condivisa di una concettualizzazione, espressa nel linguaggio della logica descrittiva. Fornisce un enorme supporto per il ragionamento automatico, ad esempio, con operazioni di sussunzione (è vero $A \subseteq B$ ?), realizzazione (dato $x$, quale $P(x)$ è vero?) e di controllo automatico della consistenza. Un esempio di linguaggio per la definizione di ontologie è OWL 2 (\textit{Web Ontology Language})

{\renewcommand{\arraystretch}{1.25} \begin{center}\begin{tabular}{L|C|N|C} \arrayrulecolor{white}
			& Flat & DB & Ontologici \\
			Eterogeneità & $\pm$ & $\pm$ & + \\
			Relazioni & - & + & + \\
			Cronologia & - & - & - \\
			Incertezza & - & $\pm$ & $\pm$ \\
			Ragionamento & - & - & + \\
			Usabilità & + & + & + \\
			Efficienza & + & + & - \\
\end{tabular}\end{center}}

\paragraph{Gestione dell'Incertezza}
I dati di contesto potrebbero essere incerti come conseguenza dell'incompletezza o dell'inaccuratezza dei dati. Un modo per rappresentare l'incertezza sono le logiche probabilistiche o fuzzy. Esiste un aspetto temporale dell'incertezza, dovuto all'invecchiamento dei dati: dati vecchi potrebbero non essere più validi, per cui possiamo specificare un time-to-live o abbassare progressivamente la confidenza nei dati.
Inoltre, diverse sorgenti di informazione potrebbero fornire dati contrastanti.
