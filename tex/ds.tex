\chapter{Sistemi Distribuiti}
Un sistema distribuito è una collezione di computer che appare all'utente come un unico sistema coerente.
Il \textit{middleware} è lo strato di software che permette di astrarre il sistema e di interfacciarsi con lo stesso come se ne fosse il sistema operativo.
I principali tipi di sistemi distribuiti sono: sistemi di calcolo distribuito, sistemi informativi distribuiti e sistemi pervasivi.
Gli obiettivi di un sistema distribuito sono:\begin{itemize}
	\item Accessibilità delle risorse
	\item Trasparenza, rispetto a \begin{itemize}
		\item Accesso: nascondere differenze di rappresentazione e di modalità di accesso
		\item Località: nascondere il luogo e la macchina che gestisce una richiesta
		\item Migrazione: nascondere il trasferimento di una risorsa in altro luogo
		\item Ricollocazione: nascondere il trasferimento di una risorsa in uso
		\item Replicazione: nascondere il fatto che una risorsa sia replicata
		\item Concorrenza: nascondere la gestione contemporanea di richieste di più utenti
		\item Guasti: nascondere eventuali guasti assicurando il soddisfacimento della richiesta
	\end{itemize}
	\item Apertura, rispetto a \begin{itemize}
		\item Interoperabilità: permettere semplicemente l'utilizzo congiunto con altri sistemi
		\item Portabilità: dei dati, delle applicazioni e delle piattaforme
		\item Estensibilità
	\end{itemize}
	\item Scalabilità, tramite la decentralizzazione di \begin{itemize}
		\item Servizi
		\item Dati
		\item Algoritmi
	\end{itemize}
\end{itemize}
\newpage\noindent
\textbf{Attenzione}, in un sistema distribuito non bisogna assumere che \begin{itemize}
	\item La rete sia affidabile
	\item La rete sia sicura
	\item La rete sia omogenea
	\item La topologia di rete sia statica
	\item La latenza sia nulla
	\item La larghezza di banda sia infinita
	\item Il costo del trasporto sia nullo
	\item L'amministratore sia unico
	\item Il debugging funzioni come in un sistema centralizzato
\end{itemize}

\section{Distributed Computing Systems}
\subsection{Cluster}
Un \textit{cluster} è una collezione di nodi equipotenti collegati ad alta velocità, solitamente dotati dello stesso sistema operativo. Sono sistemi utilizzati per HPC o per l'alta disponibilità.

I cluster simmetrici hanno nodi di pari ruolo dotati tutti dello stesso software (ad esempio
\href{http://www.mosix.org}{MOSIX}, un software di gestione di cluster Linux per calcolo distribuito).

I cluster asimmetrici hanno un nodo master che gestisce la ripartizione dei job tra i nodi, alcuni esempi sono i cluster \href{http://www.beowulf.org/overview/index.html}{Beowulf}, basati su Linux, e \href{https://ai.google/research/pubs/pub43438}{Google Borg}. I cluster asimmetrici sono più utilizzati, nonostante il nodo master rischi di costituire un \textit{bottleneck} e un \textit{single-point-of-failure}.
\subsection{Grid}
I sistemi \textit{grid} sono ispirati alla rete elettrica (\textit{electric grid}), di cui gli utenti sfruttano l'energia elettrica senza preoccuparsi di dove sia stata prodotta e che i generatori e i cavi possano essere di tipi diversi: allo stesso modo, in un grid i nodi e le reti possono essere eterogenei sia a livello hardware sia software, ma l'utente ne sfrutta la potenza di calcolo senza preoccuparsi di tutto ciò.

I nodi di un grid non sono necessariamente tutti server, ma possono essere supercomputer, sistemi di archiviazione, basi di dati, sensori, \dots ed ognuno di essi può appartenere ad un'organizzazione diversa.
\subsection{Cloud}
Un \textit{cloud} è un sistema che fornisce l'accesso pervasivo, semplice e on-demand ad un insieme di risorse di calcolo riconfigurabile rapidamente, che richieda sforzo per la gestione e interazione del fornitore del servizio nella misura minore possibile.
Un cloud dispone, quindi, di un pool di risorse messe a disposizione di molti utenti sul web, i quali possono decidere di variare la quantità di risorse utilizzate, che sarà contabilizzata dal sistema.
\begin{figure} \centering
	\caption{Esempi commerciali di diversi tipi di servizi cloud}
	\label{fig:cloud}
	\ \newline
	\includegraphics[width=0.8\textwidth]{img/clouds.jpg}
\end{figure}
I servizi cloud (figura~\ref{fig:cloud}) si dividono in\begin{itemize}
	\item IaaS (\textit{Infrastructure as a Service}): il fornitore mette a disposizione l'infrastruttura del cloud, sulla quale l'utente può installare il proprio software
	\item PaaS (\textit{Platform as a Service}): il fornitore mette a disposizione l'infrastruttura del cloud e un insieme di utilità al servizio del software dell'utente (OS, storage, DB, \dots)
	\item SaaS (\textit{Software as a Service}): il fornitore mette a disposizione su cloud
\end{itemize}
Le infrastrutture cloud sono generalmente composte da più data center, ognuno dei quali ospita uno o più cluster o grid di migliaia di nodi. Rispetto all'utenza, si dividono in\begin{itemize}
	\item \textit{Private cloud}: al servizio di una sola organizzazione
	\item \textit{Community cloud}: al servizio di un gruppo di utenti con interessi comuni
	\item \textit{Public cloud}: al servizio del pubblico generale
	\item \textit{Hybrid cloud}: composto da una parte su cloud privato e una parte su cloud pubblico
\end{itemize}
\subsection{Edge}
I sistemi di \textit{edge computing} nascono dalla necessità di avere dei servizi simili ai cloud, ma più affidabili e con meno latenza. Per questo i nodi di un sistema edge sono decentralizzati e in prossimità dei sistemi di comunicazione, comunicando direttamente sulla stessa rete LAN/WAN dei nodi client: questi sistemi sono utilizzati soprattutto in ambito IoT.
\section{Distributed Information Systems}
\subsection{Distributed Databases}
I database distribuiti sono sistemi che permettono di astrarre rispetto alla distribuzione dei dati e dell'elaborazione delle query.
Alcuni esempi sono i DBMS di tipo NoSQL o NewSQL, nonché Blockchain.
\subsection{Transaction Processing Systems}
I sistemi distribuiti per l'elaborazione delle transazioni hanno come obiettivo la distribuzione della gestione delle transazioni, garantendo le proprietà ACID delle transazioni\begin{itemize}
	\item \textit{Atomicity}: vista dall'utente, ogni transazione è indivisibile
	\item \textit{Consistency}: la transazione non viola i vincoli di consistenza del sistema
	\item \textit{Isolation}: una transazione non interferisce con una transazione concorrente
	\item \textit{Durability}: una transazione eseguita con successo ha effetto permanente
\end{itemize}
\section{Architetture}
L'architettura di un sistema distribuito definisce le entità coinvolte (processi/thread, nodi, sensori, componenti, servizi, \dots ), i rispettivi ruoli, le modalità di estensione e come sono distribuiti nell'infrastruttura fisica. Inoltre definisce i pattern e i mezzi di comunicazione.
\subsubsection{Communication Patterns}
Esistono tre principali pattern di comunicazione per i sistemi distribuiti: \textit{layering} (o \textit{multi-tiering}), \textit{object-based RMI} e \textit{event-based}.

Il layering consiste nel ripartire i compiti tra diversi strati di software organizzati a pila: il flusso di comunicazione in entrata risale la pila, mentre il flusso in uscita segue il verso opposto. Ogni componente software può risiedere in una macchina diversa, ottenendo una distribuzione dell'elaborazione delle richieste.

I pattern di comunicazione basati su oggetti si basano sull'astrazione dei componenti del sistema come oggetti del paradigma di programmazione \textit{object-oriented}. I diversi oggetti possono, però, risiedere in macchine diverse: le chiamate ai metodi sono, quindi, chiamate remote e prendono il nome di RMI (\textit{Remote Method Invocation}).

Un pattern di comunicazione basato su eventi (come \textit{publish/subscribe}) prevede che i componenti del sistema possano innescare degli eventi su un bus logico, i quali vengono rilevati dai componenti in ascolto su quel bus. Ad esempio, in un pattern publish/subscribe i \textit{publisher} producono dei messaggi che riguardano dei \textit{topic}: essi verranno recapitati a quei \textit{subscriber} che si sono iscritti allo specifico argomento.
\subsection{Client-Server}
Le architetture client-server sono modelli di sistemi centralizzati: in questi sistemi il client è un nodo che invia una richiesta, la quale è processata dal server, che restituisce una risposta.

Un modo per distribuire la computazione in un sistema client-server è quello di utilizzare il multi-tiering: in molti casi è possibile fare svolgere da diversi componenti diversi compiti che sono di competenza del server.
Questo tipo di distribuzione è detto \textit{verticale}, perché vengono distribuiti gli strati diversi della pila.

Ad esempio, per un sito web è possibile separare tre diversi livelli: interfaccia utente, elaborazione delle richieste (web server) e gestione dei dati (DBMS). Inoltre, è possibile distribuire parte del carico computazionale anche sul client: tipicamente l'interfaccia utente è sul client. Anche parte dell'applicativo web può essere eseguito sul client, ad esempio, sfruttando l'esecuzione \textit{client-side} di codice javascript. È possibile far memorizzare in cache al client anche una parte dei dati che gli possono servire, in modo che non sia necessario interrogare il DBMS per ogni richiesta.

Le cache, oltre che sul client, possono essere distribuite in multi-tiering facendo appoggio su server \textit{proxy}.

La distribuzione \textit{orizzontale}, al contrario, consiste nel distribuire lo stesso compito tra più server. Ad esempio, al posto di un DMBS server centralizzato si può utilizzare un database distribuito, al posto di un server front-end, un cluster server, \dots

Il massimo della distribuzione, consiste, dunque, nel distribuire il lavoro il più possibile in entrambe le direzioni.
\subsection{Peer-to-Peer}
Le architetture P2P sono modelli di sistemi decentralizzati, nei quali tutti i nodi hanno le stesse funzionalità e condividono le proprie risorse: in generale, le operazioni non sono coordinate da un amministratore. I problemi generali del peer-to-peer sono la distribuzione bilanciata delle risorse nei vari host, garantendo l'accesso ad ogni risorsa e con il minimo overhead. Gli obiettivi di un middleware P2P sono la trasparenza nel localizzare, recuperare, aggiungere e rimuovere risorse, nonché nel localizzare, aggiungere e rimuovere peer. Inoltre deve garantire la scalabilità, l'ottimizzazione tramite il load balancing e i criteri di località, il supporto alla sicurezza e alla privacy (cifratura, autenticazione, anonimità, \dots ).
\paragraph{Napster}è stato uno dei primi sistemi P2P di successo, nato per la condivisione di file musicali: in realtà, non è un sistema puramente peer-to-peer, ma ci sono dei \textit{Napster Index Server}. Il funzionamento del sistema è il seguente:
\begin{enumerate}
	\item Un peer client chiede ad un \textit{Napster Index Server} dove si trova un file
	\item Il server risponde con una lista di peer che offrono il file
	\item Il peer richiede il file ai peer indicati dal server
	\item Un peer fornisce il file
	\item Il peer client comunica di aver ottenuto il file al server, che aggiorna i propri registri
\end{enumerate}
\paragraph{Gnutella}è stato il primo sistema di successo ad essere completamente peer-to-peer: la conoscenza del sistema, anziché essere centralizzata in dei server, è distribuita. Ogni nodo conosce le risorse dei propri vicini, a cui inoltra in flooding le proprie richieste: tipicamente il time-to-live è di 6 o 7 hop, per evitare congestioni.
\paragraph{FreeNet}introduce nei sistemi peer-to-peer l'anonimità.
\paragraph{bitTorrent}compie un ulteriore passo nella distribuzione delle risorse: queste non devono più essere memorizzate interamente sugli host, ma è sufficiente che ogni host ne memorizzi qualche frammento. Un client che fa richiesta di una risorsa può, quindi, recuperare diversi frammenti del file richiesto da diversi host e, una volta che ne ha ottenuti alcuni lui stesso, può metterli a disposizione a sua volta.

\subsubsection{Overlay Network}
Una \textit{overlay network} è una rete logica che rappresenta i canali di comunicazione tra nodi di un sistema distribuito (tipicamente un sistema P2P) sopra una rete fisica esistente (come può essere una LAN/WAN, oppure Internet).
La presenza di un overlay permette di effettuare il routing a livello di applicazione o di middleware per localizzare una risorsa nel sistema: sono necessari degli identificatori globali che permettano di risalire ad un'istanza della risorsa identificata.
I protocolli di overlay devono anche definire le modalità di inserimento e rimozione di risorse e nodi.
Le overlay network possono essere\begin{itemize}
	\item Strutturate: la rete è costruita deterministicamente ed esiste una funzione efficiente che consenta di effettuare il routing
	\item Non-strutturate: la rete è costruita con algoritmi di instradamento probabilistici, in cui ogni nodo conosce solo i propri vicini
\end{itemize}
Il problema principale degli overlay strutturati è l'alto costo di mantenimento delle strutture dati che memorizzano lo stato della rete: per questo gli overlay non-strutturati sono basati su algoritmi di \textit{gossiping} basati sullo scambio di informazioni tra vicini in modo probabilistico. Con overlay non-strutturati non si ha la completa garanzia di successo del routing.
\paragraph{Distributed Hash Tables} è un esempio di overlay network strutturata. Lo spazio di indirizzamento di nodi $N$ e risorse $R$ è lo stesso e sono gli indici da $0$ a $2^M-1$.

Ogni risorsa è gestita dal nodo con indice successivo al proprio, definito il nodo successivo di un indice come $$succ: [0,2^M)\cap\mathbb{N} \rightarrow N, i \mapsto \argmin_{n\in N}\left[n-i\right]_{2^M}$$
\begin{figure} \centering
	\caption{Compilazione delle entry di una finger table (sinistra) e algoritmo di ricerca su distributed hash table (destra)}
	\label{fig:hashtable}
	\ \newline
	\includegraphics[width=\textwidth,trim={0 1.1in 0 0},clip]{img/dhashtab.png}
\end{figure}
La ricerca lineare delle risorse è troppo costosa, per cui ogni nodo memorizza una \textit{finger table} di $M$ righe: la tabella del nodo $n$ contiene gli indirizzi del nodo $succ(n+2^i)$ per ogni riga, il cui indice $i$ varia tra $0$ e $m-1$. L'algoritmo di ricerca è il seguente 
\lstinputlisting{./cod/hashtablelookup.code}
\paragraph{CAN} è un overlay network con spazio di indirizzamento bidimensionale, in cui ogni nodo gestisce il proprio intorno rettangolare di indirizzi.

\section{Protocolli Middleware}
I protocolli di livello middleware si inseriscono nello stack protocollare di rete come strato intermedio tra il livello trasporto e il livello applicazione. Gli obiettivi principali del livello middleware sono la persistenza e la sincronizzazione della comunicazione.
\begin{figure} \centering
	\caption{Diagramma riassuntivo dei vari tipi di comunicazione in un sistema distribuito: le storage facility garantiscono la persistenza, inoltre, sono mostrati i vari punti di sincronizzazione}
	\label{fig:persynch}
	\ \newline
	\includegraphics[width=0.8\textwidth]{img/persynch.jpg}
\end{figure}
\subsubsection{Persistenza}
La comunicazione in un sistema è persistente se un messaggio, una volta inviato, è memorizzato dal middleware (figura~\ref{fig:persynch}) finché non viene ricevuto dal destinatario. Il sistema di storage su cui si appoggia il middleware per memorizzare i messaggi è, tipicamente, un sistema distribuito di centri di storage.

Il vantaggio di un sistema dotato di persistenza è che il destinatario non deve necessariamente essere acceso ed in rete quando il messaggio viene inviato: esempi comuni di sistemi a comunicazione persistente sono i sistemi di posta elettronica.

Al contrario, in un sistema con comunicazione transiente il middleware non è in grado di consegnare un messaggio se avviene un errore di trasmissione o se il destinatario non è attivo, perdendolo. I servizi di comunicazione che si appoggiano solo sul livello di trasporto non garantiscono la persistenza.
\subsubsection{Sincronizzazione}
In un sistema di comunicazione asincrona, il mittente riprende ad eseguire subito dopo aver inviato il messaggio.

Il middleware può fornire supporto per la sincronizzazione: in tal caso, il mittente è bloccato fino a che non riceve conferma dell'accettazione del suo messaggio. La sincronizzazione può avvenire rispetto a eventi diversi (figura~\ref{fig:persynch}):\begin{itemize}
	\item \textit{Recepit-based synchronization}: il mittente attende la conferma che il middleware abbia ricevuto il messaggio
	\item \textit{Delivery-based synchronization}: il mittente attende la conferma che il destinatario abbia ricevuto il messaggio
	\item \textit{Response-based synchronization}: il mittente attende che il destinatario processi il messaggio e invii una risposta
\end{itemize}

\subsection{Message Oriented Communication}
\subsubsection{Message Oriented Transient Communication}
\begin{figure} \centering
	\caption{Pattern di comunicazione orientata alla connessione con TCP socket}
	\label{fig:sockloop}
	\ \newline
	\includegraphics[width=0.9\textwidth]{img/sockloop.jpg}
\end{figure}
La comunicazione orientata ai messaggi transiente si appoggia direttamente sul livello trasporto, utilizzando le \textit{Berkeley socket} (figura~\ref{fig:sockloop}). Le primitive per l'uso delle socket TCP sono:\begin{itemize}
	\item \texttt{Socket}: inizializza un \textit{end point} per la comunicazione
	\item \texttt{Bind}: collega la socket ad un indirizzo
	\item \texttt{Listen}: mette la socket in attesa di connessioni
	\item \texttt{Accept}: blocca il chiamante finché non arriva una richiesta di connessione
	\item \texttt{Connect}: cerca attivamente di stabilire una connessione
	\item \texttt{Send}/\texttt{Write}: invia/scrive dati sul canale di comunicazione
	\item \texttt{Receive}/\texttt{Read}: riceve/legge dati dal canale di comunicazione
	\item \texttt{Close}: chiude e libera il canale di comunicazione
\end{itemize}
\subsubsection{Message Oriented Persistent Communication}
\begin{figure} \centering
	\caption{Sistema distribuito con middleware orientato ai messaggi}
	\label{fig:mom}
	\ \newline
	\includegraphics[width=0.7\textwidth,trim={0 0 0 0.05in},clip]{img/mom.jpg}
\end{figure}
I \textit{Message-queuing system} (detti anche MOM, \textit{Message-Oriented Middleware}) sono sistemi per la comunicazione persistente orientata ai messaggi: le applicazioni comunicano immettendo e estraendo messaggi da una coda. Le comunicazioni sono, in questo modo, \textit{loosely coupled}: viene garantita la consegna del messaggio, ma non un limite di tempo.

Il mittente e il destinatario hanno due code ciascuno: una in cui inseriscono i messaggi in uscita (\textit{send queue}) e una da cui estraggono i messaggi in entrata (\textit{receive queue}). I router si occupano di trasferire i messaggi tra le code, eventualmente fornendo servizi per la persistenza e la sincronizzazione: anche loro hanno due code di messaggi. Le applicazioni che gestiscono le code sono dette \textit{queue managers}: le primitive che sfruttano sono:\begin{itemize}
	\item \texttt{Put}: inserisce un messaggio in coda
	\item \texttt{Get}: preleva il primo elemento di una coda (bloccante)
	\item \texttt{Poll}: se la coda non è vuota preleva il primo elemento (non-bloccante)
	\item \texttt{Notify}: istanzia un handler per ricevere notifiche quando un messaggio entra in coda
\end{itemize}
Un problema dei sistemi distribuiti, specialmente quelli derivati dall'estensione di sistemi preesistenti è che i componenti possono supportare diversi formati di messaggio: nei MOM, le conversioni di formato sono gestiti da nodi speciali nella rete, chiamati \textit{message broker}. Un message broker ha il ruolo di gateway di livello applicazione: le operazioni possono essere anche più complesse della semplice conversione di formato.

\subsection{Remote Procedure Call}
\begin{figure} \centering
	\caption{Pattern di binding di un client a un server in una comunicazione basata su RPC}
	\label{fig:rpcbind}
	\ \newline
	\includegraphics[width=0.9\textwidth]{img/rpcbind.jpg}
\end{figure}
La comunicazione orientata ai messaggi obbliga a gestire esplicitamente e abbastanza a basso livello la comunicazione tra nodi, limitando fortemente la trasparenza. La comunicazione basata su RPC (\textit{Remote Procedure Call}) consiste nel permettere ad un processo su un nodo di chiamare una procedura su un altro nodo.

Ciò che viene realmente chiamato dal client è un \textit{client stub}: questo stub si occupa del marshalling dei parametri e dell'invio di tali parametri al server, sfruttando le chiamate di sistema locali.

Analogamente, lato server viene chiamato dal sistema operativo un \textit{server stub}: questo stub fa un-marshalling dei parametri per passarli alla procedura locale. Il risultato della procedura locale viene passato al server stub, che ne fa il marshalling e lo consegna al sistema operativo per spedirlo al client. A questo punto, solitamente, il server stub si rimette in attesa di chiamate da altri client.

Il sistema operativo del client consegna il risultato a client stub, che fa un-marshalling e lo restituisce alla procedura chiamante.

Di tutto questo scambio di messaggi le procedure del client e del server sono completamente ignari: addirittura, esse potrebbero essere state scritte in linguaggi di programmazione diversi e eseguiti su macchine diverse con sistemi operativi diversi. Un problema a cui prestare attenzione è il passaggio dei parametri per riferimento: il server non può accedere alla memoria riferita da un puntatore fornito dal client, quindi, il client deve estrarre i parametri dalla memoria puntata e inviarli per valore al server.

\subsubsection{Sincronizzazione}
Le RPC possono essere sincrone: in tal caso, il client attende che il server risponda con il risultato della procedura. Nel caso di RPC asincrone, invece, il client continua la propria esecuzione dopo aver ricevuto la conferma di accettazione della chiamata, senza attendere il completamento della procedura.

\subsubsection{Interface Definition Language}
Un IDL (\textit{Interface Definition Language}) è un linguaggio usato per definire l'interfaccia tra processi client e server in un sistema distribuito. Ogni IDL ha un insieme di compilatori che permettono di generare gli stub delle procedure, che implementano la comunicazione di basso livello a supporto delle RPC. L'output della compilazione di un'interfaccia è composto da tre file: un header (es. in C un file \texttt{.h}), un client stub e un server stub.

\subsubsection{Binding}
Il collegamento di un client ad un server avviene in questo modo (figura~\ref{fig:rpcbind})\begin{enumerate}
	\item Il server si registra presso un demone in locale per garantire la continua reperibilità
	\item Il server si registra presso un \textit{directory server} 
	\item Il client interroga il directory server per ottenere l'indirizzo del demone
	\item Il client interroga il demone per ottenere l'indirizzo (la porta) del server
	\item Il client invoca la procedura remota
\end{enumerate}

\subsubsection{Object-Orientation}
Nell'ambito della programmazione ad oggetti, la comunicazione tramite RPC si declina nell'invocazione a metodi remoti (RMI): in questo caso, gli oggetti sono su server ed espongono le interfacce di alcuni metodi che possono essere invocati da remoto. Un vantaggio di questo paradigma è, ad esempio, che lo stato dell'oggetto rimane memorizzato su server, per cui non deve essere ritrasmesso ogni volta dal client. Un esempio è Java RMI.

\subsection{Stream Oriented Communication}
La \textit{stream oriented communication} è una tipologia di comunicazione adottata in sistemi che trattano flussi continui di dati multimediali. Essi vengono trattati garantendo alcune proprietà di  qualità del servizio di rete (QoS):\begin{itemize}
	\item Bitrate per il trasporto dei dati
	\item Ritardo massimo per la creazione di una sessione
	\item Ritardo massimo \textit{end-to-end}
	\item Massimo \textit{round-trip-time}
	\item Massima varianza nel ritardo (\textit{jitter})
\end{itemize}
Per evitare un elevato jitter, solitamente viene effettuato il buffering dello stream in ingresso, in modo da avere più margine per gestire ritardi maggiori rispetto alla media. Inoltre, per evitare la perdita lunghe sequenze contigue di dati, si può utilizzare l'\textit{interleaving}. La sincronizzazione dei contenuti multimediali viene svolta a livello applicazione, ad esempio utilizzando i metadati in uno stream in formato MPEG.

\section{Robustezza ai Guasti}
Perché un sistema sia robusto ai guasti deve essere\begin{itemize}
	\item Disponibile (\textit{Available}): la probabilità che il sistema funzioni correttamente in un determinato momento
	\item Affidabile (\textit{Reliable}): la probabilità che il sistema funzioni correttamente per un periodo prolungato di tempo
	\item Sicuro (\textit{Safe}): i guasti non provocano effetti catastrofici
	\item Mantenibile (\textit{Maintainable}) : i guasti sono facilmente riparabili
\end{itemize}

\subsection{Failure}
Esistono tipi diversi di \textit{failure}: \begin{itemize}
	\item \textit{Crash failure}: un nodo si arresta, ma fino all'arresto funzionava correttamente
	\item \textit{Omission failure}: un nodo non risponde alle richieste \begin{itemize}
		\item \textit{Receive omission}: il nodo fallisce nel ricevere messaggi
		\item \textit{Send omission}: il nodo fallisce nell'inviare messaggi
	\end{itemize}
	\item \textit{Response failure}: un nodo invia risposte errate \begin{itemize}
		\item \textit{Value failure}: il valore della risposta è errato
		\item \textit{State-transition failure}: il nodo devia dal flusso di esecuzione corretto
	\end{itemize}
\item \textit{Arbitrary failure}: il nodo produce risposte arbitrarie in tempi arbitrari (comportamento bizantino) ed è potenzialmente malizioso
\end{itemize}

\subsection{Ridondanza}
Una strategia per contrastare i guasti è quella di utilizzare la ridondanza: \begin{itemize}
	\item Ridondanza di Informazione: si utilizza memoria aggiuntiva per recuperare le informazioni in seguito a degrado, ad esempio con dei codici a correzione di errore
	\item Ridondanza Temporale: se una richiesta non viene soddisfatta a causa di un guasto, essa viene ripetuta in un secondo momento
	\item Ridondanza Fisica: hardware (o software) aggiuntivo viene impiegato per garantire la resistenza ai guasti dei componenti
	\item Ridondanza di Processi: ogni compito viene svolto da un gruppo di processi. È necessario un protocollo per gestire l'entrata e l'uscita da un gruppo e per risolvere i conflitti nei risultati (es. il risultato del gruppo è il risultato della maggioranza dei processi)
\end{itemize}
Per \textit{crash failure} è sufficiente una ridondanza di $k+1$ per proteggersi dal guasto di $k$ repliche. In caso di \textit{value failure} sono necessari $2k+1$ repliche: il client può decidere il valore corretto tramite votazione. Per problemi più complicati serve un algoritmo di consenso.

\subsection{Consenso}
Gli algoritmi distribuiti di consenso hanno come scopo quello di mettere d'accordo su un problema tutti i processi. Si distinguono casi diversi, a seconda che il sistema sia sincrono o asincrono, che il ritardo di comunicazione abbia un limite massimo garantito o meno, che la consegna dei messaggi sia ordinata nello stesso ordine dell'invio o no, che la comunicazione sia unicast o multicast.

\subsubsection{Algoritmo di Lamport}
L'algoritmo di Lamport per il consenso in presenza di comportamento bizantino funziona sotto alcune condizioni: i processi sono sincroni, la comunicazione è unicast, il ritardo è limitato, l'ordine dei messaggi è rispettato.

Ogni processo invia il proprio valore agli altri: il processo bizantino potrà inviare dei valori arbitrari. Ogni processo organizza un vettore con i risultati di ogni processo, dopodiché lo invia agli altri: il processo bizantino potrà sempre inviare vettori con valori arbitrari. Ogni processo esamina gli elementi dei vettori ricevuti: se per una posizione $i$ del vettore esiste una maggioranza, allora l'elemento $i$-esimo del vettore risultato è il valore di maggioranza, altrimenti è sconosciuto.

Per ottenere un consenso sui valori calcolati dai processi sani, in presenza di $k$ processi bizantini, sono necessari $3k+1$ processi in totale.
